package com.myst3ry.asyncworktask;

/**
 * A class which helps to set Callable to the parent class {@link MyObservable}
 *
 * @param <T> any type
 */

public final class MyObservableFromCallable<T> extends MyObservable<T> {

    public MyObservableFromCallable(final MyCallable<? extends T> callable) {
        this.callable = callable;
    }
}
