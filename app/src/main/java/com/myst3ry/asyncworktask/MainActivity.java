package com.myst3ry.asyncworktask;

import android.os.Bundle;
import android.os.HandlerThread;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.util.ArrayList;

public final class MainActivity extends AppCompatActivity {

    private static final String TAG_TEST = "TEST";
    private static final String HANDLER_TREAD_NAME = "WORKER_THREAD";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final HandlerThread handlerThread = new HandlerThread(HANDLER_TREAD_NAME);
        handlerThread.start();

        final Looper workerLooper = handlerThread.getLooper();
        final Looper mainLooper = getMainLooper();

        //integer test
        final MyCallable<Integer> cOne = () -> 42;
        MyObservable.from(cOne)
                .subscribeOn(workerLooper)
                .observeOn(mainLooper)
                .subscribe(result -> Log.w(TAG_TEST, String.valueOf(result)));

        //string test
        final MyCallable<String> cTwo = () -> "Text";
        MyObservable.from(cTwo)
                .observeOn(mainLooper)
                .subscribeOn(workerLooper)
                .subscribe(result -> Log.w(TAG_TEST, result));

        //arraylist test
        final ArrayList<String> list = new ArrayList<>();
        list.add("One");
        list.add("Two");
        list.add("Three");

        final MyCallable<ArrayList<String>> cThree = () -> list;
        MyObservable.from(cThree)
                .subscribeOn(workerLooper)
                .observeOn(mainLooper)
                .subscribe(result -> Log.w(TAG_TEST, result.toString()));

        //no loopers test
        final MyCallable<String> cFour = () -> "No Loopers";
        MyObservable.from(cFour)
                .subscribe(result -> Log.w(TAG_TEST, result));

        //no main looper test
        final MyCallable<String> cFive = () -> "No Observe Looper";
        MyObservable.from(cFive)
                .subscribeOn(workerLooper)
                .subscribe(result -> Log.w(TAG_TEST, result));

        //no worker looper test
        final MyCallable<String> cSix = () -> "No Subscribe Looper";
        MyObservable.from(cSix)
                .observeOn(mainLooper)
                .subscribe(result -> Log.w(TAG_TEST, result));
    }
}
