package com.myst3ry.asyncworktask;

/**
 * A simple callback interface parameterized T as any type for callback method.
 *
 * @param <T> any type
 */

public interface MyCallback<T> {

    void onCallback(T result);
}
