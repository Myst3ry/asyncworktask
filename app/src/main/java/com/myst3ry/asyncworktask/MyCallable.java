package com.myst3ry.asyncworktask;

import java.util.concurrent.Callable;

/**
 * A simple wrapper interface on Callable interface with same functions as original, but can be modified on demand.
 *
 * @param <T> any type
 */

public interface MyCallable<T> extends Callable<T> {
}
