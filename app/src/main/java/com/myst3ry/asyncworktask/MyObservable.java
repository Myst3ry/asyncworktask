package com.myst3ry.asyncworktask;


import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

/**
 * A class which implements a very simple classical Observable with {@link MyCallable},
 * {@link MyCallback} and works with loopers. Can be parameterize as any type.
 * @param <T> any type
 */

public class MyObservable<T> {

    private static final String TAG_THREAD = "THREAD";
    private static final int MSG_CALL_ID = 99;

    MyCallable<? extends T> callable;
    private Looper workerLooper;
    private Looper mainLooper;

    public static <T> MyObservable<T> from(final MyCallable<? extends T> callable) {
        return new MyObservableFromCallable<>(callable);
    }

    public MyObservable<T> subscribeOn(final Looper workerLooper) {
        this.workerLooper = workerLooper;
        return this;
    }

    public MyObservable<T> observeOn(final Looper mainLooper) {
        this.mainLooper = mainLooper;
        return this;
    }

    public void subscribe(final MyCallback<T> callback) {

        if (workerLooper == null && mainLooper == null) {
            workerLooper = Looper.myLooper();
            mainLooper = Looper.myLooper();
        } else if (workerLooper == null) {
            workerLooper = Looper.myLooper();
        } else if (mainLooper == null) {
            mainLooper = workerLooper;
        }

        final Handler workerHandler = new Handler(workerLooper);
        final Handler mainHandler = new Handler(mainLooper) {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == MSG_CALL_ID) {
                    //noinspection unchecked
                    callback.onCallback((T) msg.obj);
                }
            }
        };

        workerHandler.post(() -> {
            try {
                final Message message = new Message();
                message.obj = callable.call();
                message.what = MSG_CALL_ID;
                mainHandler.sendMessage(message);

                Log.w(TAG_THREAD, String.format("Thread: %s # Result: %s",
                        Thread.currentThread().getName().toUpperCase(),
                        message.obj.toString()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        final Message message = mainHandler.obtainMessage();
        mainHandler.handleMessage(message);
    }
}